<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => str_random(10),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Models\Category::class, function ($faker) {
    return [
        "title"=>$faker->word,
        "description"=>$faker->text
    ];
});
$factory->define(App\Models\Video::class, function ($faker) {
    return [
        'category' => $faker->numberBetween($min = 1, $max = 6),
        'user'     => $faker->numberBetween($min = 1, $max = 10),
        'title'    => $faker->title,
        'win'      => $faker->boolean($chanceOfGettingTrue = 3),
        'video_id' => $faker->md5,
        'start'    => $faker->randomNumber(),
        'pause'    => $faker->randomNumber(),
        'stop'     => $faker->randomNumber()

    ];
});

