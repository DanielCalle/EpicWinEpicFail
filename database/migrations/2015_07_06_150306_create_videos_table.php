<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category')->unsigned();
            $table->integer('user')->unsigned();
            $table->string('title');
            $table->boolean('win');
            $table->string('video_id');
            $table->integer('start');
            $table->integer('pause');
            $table->integer('stop');
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->foreign('category')->references('id')->on('categories');
            $table->foreign('user')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }
}
