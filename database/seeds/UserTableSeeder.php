<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class)->create([
            'email'=>'danielcallesanchez@gmail.com',
            'name'=>'admin',
            'password'=>Hash::make('1234'),
            'role'=>'admin'
        ]);
        factory('App\Models\User', 10)->create();
    }
}
