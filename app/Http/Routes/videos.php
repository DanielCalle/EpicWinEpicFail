<?php

Route::resource('video','VideoController',
    ['only' => ['index', 'show']]);

Route::get('video/category/{id}','VideoController@showByCategory');

Route::get('video/user/{id}','VideoController@showByUser');

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('video','VideoController',
        ['only' => ['update', 'create']]);
});