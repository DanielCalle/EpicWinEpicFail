<?php

Route::post('login', 'UserController@login');

Route::get('user', ['middleware' => 'jwt.auth', 'uses'=>'UserController@getUser']);

Route::post('register', 'UserController@register');