<?php

Route::resource('category','CategoryController',
    ['only' => ['index', 'show']]);

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('category','CategoryController',
        ['only' => ['update', 'create']]);
});