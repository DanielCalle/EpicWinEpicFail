<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Category;
use Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::all();
        return Response::json(compact('categories'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $user = JWTAuth::parseToken()->toUser();
        $credentials = Input::all()['category'];

        try {
            $category = Category::create($credentials);
            return  Response::json(compact('category'),200);
        } catch (Exception $e) {
            return Response::json(['error' => 'Category already exists.'],409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if(! $category = Category::find($id) )
            return Response::json(['error'=>'Not Found'],404);

        return Response::json(compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $credentials = Input::all()['category'];
        //return Response::json(['error' => 'Unauthorized'], 401);

        $category->update($credentials);;
        return Response::json(compact('category'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(! $category = Category::find($id) )
            return Response::json(['error'=>'Not Found'],404);
        $category->delete();
        return Response::json(['success'=>'true'],200);
    }
}
