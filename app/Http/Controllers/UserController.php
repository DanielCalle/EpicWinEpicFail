<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\User;
use App\Http\Requests;
use Response;
use Input;
use JWTAuth;
use Exception;

class UserController extends Controller
{
    public function login(Response $response){

        // grab credentials from the request
        $credentials = Input::all()['user'];

        try {

            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return Response::json(['error' => 'invalid_credentials'], 401);
            }

        } catch (JWTException $e) {

            // something went wrong whilst attempting to encode the token
            return Response::json(['error' => 'could_not_create_token'], 500);

        }

        // all good so return the token
        return Response::json(compact('token'));

    }

    public function getUser(){

        $user = JWTAuth::parseToken()->toUser();

        return Response::json(compact('user'));

    }

    public function register(){
        $credentials = Input::all()['user'];
        $credentials['password']=Hash::make($credentials['password']);
        try {
            $user = User::create($credentials);
        } catch (Exception $e) {
            return Response::json(['error' => 'User already exists.'], 409);
        }

        $token = JWTAuth::fromUser($user);

        return Response::json(compact('token'));
    }
}
