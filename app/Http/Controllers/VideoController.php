<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Video;
use App\Http\Requests;
use Response;
use Input;
use JWTAuth;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $videos = Video::all();
        return Response::json(compact('videos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $user = JWTAuth::parseToken()->toUser();
        $credentials = Input::all()['video'];
        $credentials['user']=$user->id;

        try {

            $video = Video::create($credentials);
            return  Response::json(compact('video'),200);
        } catch (Exception $e) {
            return Response::json(['error' => 'Video already exists.'],409);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if(! $video = Video::find($id) )
            return Response::json(['error'=>'Not Found'],404);

        return Response::json(compact('video'));
    }

    /**
     * Show a category's videos
     *
     * @param  int  $id
     * @return Response
     */
    public function showByCategory($id)
    {
        if(! $videos = Video::where('category',$id)->get() )
            return Response::json(['error'=>'Not Found'],404);

        return Response::json(compact('videos'));
    }

    /**
     * Show a user's videos
     *
     * @param  int  $id
     * @return Response
     */
    public function showByUser($id)
    {
        if(! $videos = Video::where('user',$id)->get() )
            return Response::json(['error'=>'Not Found'],404);

        return Response::json(compact('videos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $user = JWTAuth::parseToken()->toUser();
        $credentials = Input::all()['video'];
        $video = Video::find($id);
        if ($video->user != $user->id)
            return Response::json(['error' => 'Unauthorized'], 401);

        $video->update($credentials);;
        return Response::json(compact('video'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(! $video = Video::find($id) )
            return Response::json(['error'=>'Not Found'],404);
        $video->delete();
        return Response::json(['success'=>'true'],200);
    }
}
